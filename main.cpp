/**
 * Christos Gogos (2014)
 *
 *	30/10/2014: Κώδικας για ανάγνωση δεδομένων
 *
 */

#include <fstream>
#include <iostream>
#include <cstdlib>
#include <sstream>
#include <list>
#include <set>
#include <unordered_map>
// BOOST
#include <boost/lambda/lambda.hpp>
#include <iterator>
#include <utility>
#include <algorithm>
#include <boost/config.hpp>
#include <boost/property_map/property_map.hpp>
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <boost/graph/properties.hpp>

using namespace std;

int stu_no = 0; // αριθμός σπουδαστών
int crs_no = 0; // αριθμός μαθημάτων
int per_no = 0; // αριθμός περιόδων εξέτασης (δίνεται στην εκφώνηση)
int* crs_per_stu_n; // πίνακας με αριθμό μαθημάτων ανά σπουδαστή
int** crs_per_stu; // 2Δ πίνακας με τους αριθμούς των μαθημάτων ανά σπουδαστή
int* stu_per_crs_n; // πίνακας με τον αριθμό των σπουδαστών ανά μάθημα
int** stu_per_crs; // 2Δ πίνακας με τους αριθμούς των σπουδαστών ανά μάθημα
int** conflict_matrix; // 2Δ πίνακας συγκρούσεων
list<pair<int, int>>* graph; // γράφημα προβλήματος με κορυφές τα μαθήματα και ακμές με βάρος τον αριθμό των κοινών σπουδαστών
string problem_name;
string sol_fn; // όνομα αρχείου με λύση
int* sol; // πίνακας με περίοδο στην οποία τοποθετείται η κάθε εξέταση
unordered_map<int, int> um_sol; //hash table

void test1() {
	for (int i = 0; i < stu_no + 1; i++)
		printf("%d %d\n", i, crs_per_stu_n[i]);
}

void test2() {
	for (int i = 0; i < crs_no + 1; i++)
		printf("%d %d\n", i, stu_per_crs_n[i]);
}

void test3() {
	for (int i = 1; i < stu_no + 1; i++) {
		printf("Student %d :", i);
		for (int j = 0; j < crs_per_stu_n[i]; j++)
			printf("%d ", crs_per_stu[i][j]);
		printf("\n");
	}
}

void test4() {
	for (int i = 1; i < crs_no + 1; i++) {
		printf("Course %d :", i);
		for (int j = 0; j < stu_per_crs_n[i]; j++)
			printf("%d ", stu_per_crs[i][j]);
		printf("\n");
	}
}

void print_conflict_density() {
	int c = 0;
	for (int i = 1; i < crs_no + 1; i++)
		for (int j = 1; j < crs_no + 1; j++)
			if (conflict_matrix[i][j] == 1)
				c++;
	double conflictDensity = (double) c / (crs_no * crs_no);
	cout << "Conflict density is " << conflictDensity << endl;
}

int count_enrolments() {
	int c = 0;
	for (int i = 1; i <= crs_no; i++) {
		c += stu_per_crs_n[i];
	}
	return c;
}

int count_enrolments2() {
	int c = 0;
	for (int i = 1; i <= stu_no; i++) {
		c += crs_per_stu_n[i];
	}
	return c;
}

void read_problem_data(string problem) {
	problem_name = problem;
	string stu_fn = "";
	if (problem == "TINY") {
		stu_fn = "tiny.stu";
		stu_no = 10;
		crs_no = 5;
		per_no = 5;
		sol_fn = "tiny.sol";
	} else if (problem == "CAR91-II") {
		stu_fn = "car-s-91-II.stu";
		stu_no = 16925;
		crs_no = 682;
		per_no = 35;
		sol_fn = "car-s-91.sol";
	} else if (problem == "CAR91") {
		stu_fn = "car-s-91.stu";
		stu_no = 16925;
		crs_no = 682;
		per_no = 35;
		sol_fn = "car-s-91.sol";
	} else if (problem == "KFU93") {
		stu_fn = "kfu-s-93.stu";
		stu_no = 5349;
		crs_no = 461;
		per_no = 20;
		sol_fn = "kfu-s-93.sol";
	} else if (problem == "LSE91") {
		stu_fn = "lse-f-91.stu";
		stu_no = 2726;
		crs_no = 381;
		per_no = 18;
		sol_fn = "lse-f-91.sol";
	} else if (problem == "TRE92") {
		stu_fn = "tre-s-92.stu";
		stu_no = 4360;
		crs_no = 261;
		per_no = 23;
//		sol_fn = "tre-s-92.sol";
		sol_fn = "tre92.sol"; // cost should be 7.75
	} else if (problem == "UTE92") {
		stu_fn = "ute-s-92.stu";
		stu_no = 2750;
		crs_no = 184;
		per_no = 10;
//		sol_fn = "ute-s-92.sol";
		sol_fn = "ute-s-92-gogos.sol"; // cost 27.2156
//		sol_fn = "ute92.sol";
	} else {
		cerr << "Not implemented";
		exit(-1);
	}

	crs_per_stu_n = new int[stu_no + 1](); // δημιουργία πίνακα και αρχικοποίηση με 0
	crs_per_stu = new int*[stu_no + 1];
	stu_per_crs_n = new int[crs_no + 1]();
	stu_per_crs = new int*[crs_no + 1]();
	conflict_matrix = new int*[crs_no + 1]();
	for (int i = 0; i < crs_no + 1; i++) {
		conflict_matrix[i] = new int[crs_no + 1]();
	}

	fstream filestr;
	string buffer;
	filestr.open(stu_fn.c_str());
	int stu_index = 1;
	if (filestr.is_open()) {
		while (getline(filestr, buffer)) {
			string buffer2;
			stringstream ss;
			ss.str(buffer); // αρχικοποίηση stringstream
			int crs = 0; // αριθμός μαθημάτων στα οποία θα εξεταστεί ο σπουδαστής
			while (ss >> buffer2) {
				int crs_index = atoi(buffer2.c_str());
				crs++;
				stu_per_crs_n[crs_index]++;
			}
			crs_per_stu_n[stu_index] = crs;
			crs_per_stu[stu_index] = new int[crs]();

			ss.clear();
			ss.str(buffer);
			int j = 0;
			while (ss >> buffer2) {
				int crs_index = atoi(buffer2.c_str());
				crs_per_stu[stu_index][j] = crs_index;
				j++;
			}
			stu_index++;
		}
		filestr.close();

// δημιουργία πίνακα crs_per_stu
		for (int i = 1; i < crs_no + 1; i++) {
			int students = stu_per_crs_n[i];
			stu_per_crs[i] = new int[students]();
		}
		int temp[crs_no + 1] = { 0 };
		for (int i = 1; i < stu_no + 1; i++) {
			for (int j = 0; j < crs_per_stu_n[i]; j++) {
				int k = crs_per_stu[i][j];
				stu_per_crs[k][temp[k]] = i;
				temp[k]++;
			}
		}

// δημιουργία πίνακα conflict_matrix
		for (int exam1 = 1; exam1 < crs_no + 1; exam1++) {
			for (int exam2 = exam1 + 1; exam2 < crs_no + 1; exam2++) {
				bool f = true;
				for (int i = 0; i < stu_per_crs_n[exam1] && f; i++)
					for (int j = 0; j < stu_per_crs_n[exam2] && f; j++)
						if (stu_per_crs[exam1][i] == stu_per_crs[exam2][j]) {
							conflict_matrix[exam1][exam2] = 1;
							conflict_matrix[exam2][exam1] = 1;
							f = false;
						}
			}
		}
	} else {
		cout << "Error opening file: " << stu_fn << endl;
		exit(-1);
	}
}

void student_stats() {
	double sum = 0.0;
	int pmax = 1;
	for (int i = 1; i <= stu_no; i++) {
		sum += crs_per_stu_n[i];
		if (crs_per_stu_n[i] > crs_per_stu_n[pmax])
			pmax = i;
	}
	double avg = sum / stu_no;
	printf("Μέσος όρος δηλωθέντων μαθημάτων ανά σπουδαστή = %.2f\n", avg);
	printf("Μεγαλύτερος αριθμός δηλωθέντων μαθημάτων ανά σπουδαστή = %d\n",
			crs_per_stu_n[pmax]);
	int f[crs_per_stu_n[pmax] + 1] = { 0 };
	for (int i = 0; i <= stu_no; i++) {
		f[crs_per_stu_n[i]]++;
	}
	for (int i = 1; i <= crs_per_stu_n[pmax]; i++) {
		printf("%d σπουδαστές με %d μαθήματα\n", f[i], i);
	}
}

void courses_stats() {
	double sum = 0.0;
	int pmax = 1;
	for (int i = 1; i <= crs_no; i++) {
		sum += stu_per_crs_n[i];
		if (stu_per_crs_n[i] > stu_per_crs_n[pmax])
			pmax = i;
	}
	double avg = sum / crs_no;
	printf("Μέσος όρος σπουδαστών ανά μάθημα = %.2f\n", avg);
	printf("Μεγαλύτερος αριθμός σπουδαστών σε ένα μάθημα = %d\n",
			stu_per_crs_n[pmax]);
	int f[stu_per_crs_n[pmax] + 1] = { 0 };
	for (int i = 0; i <= crs_no; i++) {
		f[stu_per_crs_n[i]]++;
	}
	for (int i = 1; i <= stu_per_crs_n[pmax]; i++) {
		if (f[i] > 0)
			printf("%d %s με %d σπουδαστές\n", f[i],
					(f[i] == 1 ? "μάθημα" : "μαθήματα"), i);
	}
}

int count_common_students(int& crs1, int& crs2) {
	int c = 0;
	for (int i = 0; i < stu_per_crs_n[crs1]; i++) {
		for (int j = 0; j < stu_per_crs_n[crs2]; j++) {
			if (stu_per_crs[crs1][i] == stu_per_crs[crs2][j]) {
				c++;
			}
		}
	}
	return c;
}

void export_graph_to_txt(string fn) {
	ofstream ofs;
	ofs.open(fn);
	for (int i = 1; i <= crs_no; i++) {
		ofs << i << " ";
		for (pair<int, int> p : graph[i]) {
			ofs << p.first << "," << p.second << " ";
		}
		ofs << endl;
	}
	ofs.close();
}

void graph_construction() {
	graph = new list<pair<int, int>> [crs_no + 1]();
	for (int i = 1; i <= crs_no; i++) {
		for (int j = i + 1; j <= crs_no; j++) {
			int c = count_common_students(i, j);
			if (c > 0) {
//				printf(
//						"H εξέταση %d και η εξέταση %d έχουν %d κοινούς σπουδαστές\n",
//						i, j, c);
				graph[j].push_back(make_pair(c, i));
				graph[i].push_back(make_pair(c, j));
			}
		}
	}
	for (int i = 1; i <= crs_no; i++) {
		printf("Exam %d degree %llu\n", i, graph[i].size());
	}
	export_graph_to_txt(problem_name + "_GRAPH.txt");
}

void compute_shortest_paths_to_all_vertices(list<pair<int, int>>* g, int V,
		int source, int* distance) {
	list<int> S;
	set<int> V_S;
	for (int i = 1; i <= V; i++) {
		if (i == source) {
			S.push_back(i);
			distance[i] = 0;
		} else {
			V_S.insert(i);
			distance[i] = INT_MAX;
		}
	}
	while (!V_S.empty()) {
		int min = INT_MAX;
		int pmin = -1;
		for (int v1 : S) {
			for (pair<int, int> w_v : g[v1]) {
				int weight = w_v.first;
				int v2 = w_v.second;
				bool is_in_V_S = V_S.find(v2) != V_S.end();
				if ((is_in_V_S) && (distance[v1] + weight < min)) {
					min = distance[v1] + weight;
					pmin = v2;
				}
			}
		}
		// The graph might not be connected
		if (pmin == -1)
			break;
		distance[pmin] = min;
		S.push_back(pmin);
		V_S.erase(pmin);
	}
}

void print_graph(list<pair<int, int>>* g, int V) {
	int k = 1;
	for (int i = 1; i <= V; i++) {
		for (pair<int, int> p : g[i]) {
			printf("%d<--%d-->%d\t", i, p.first, p.second);
			if (k % 5 == 0)
				printf("\n");
			k++;
		}
	}
	printf("\n");
}

int graph_diameter() {
	int V = crs_no;
	// print_graph(graph, V);
	int max = 0;
	int v1 = -1, v2 = -1;
	for (int source_vertex = 1; source_vertex <= V; source_vertex++) {
		int* shortest_path_distances = new int[V + 1];
		compute_shortest_paths_to_all_vertices(graph, V, source_vertex,
				shortest_path_distances);
		for (int destination_vertex = source_vertex + 1;
				destination_vertex <= V; destination_vertex++) {
			if (shortest_path_distances[destination_vertex] == INT_MAX)
				continue;
//			printf(
//					"vertex:%d to vertex:%d shortest path distance=%d\n",
//					source_vertex, destination_vertex,
//					shortest_path_distances[destination_vertex]);
			if (shortest_path_distances[destination_vertex] > max) {
				max = shortest_path_distances[destination_vertex];
				v1 = source_vertex;
				v2 = destination_vertex;
				printf(
						"NEW MAX ==> vertex:%d to vertex:%d shortest path distance=%d\n",
						source_vertex, destination_vertex,
						shortest_path_distances[destination_vertex]);

			}
		}
		delete[] shortest_path_distances;
	}
	printf("Diameter %d (%d %d)\n", max, v1, v2);
	return max;
}

int compute_cost() {
	int sum = 0;
	for (int i = 1; i <= stu_no; i++) {
		int c = 0;
		for (int j = 0; j < crs_per_stu_n[i]; j++) {
			for (int k = j + 1; k < crs_per_stu_n[i]; k++) {
				int distance = um_sol[crs_per_stu[i][j]]
						- um_sol[crs_per_stu[i][k]];
				if (distance == 0) {
					cerr << "Student: " << i << " Courses: "
							<< crs_per_stu[i][j] << "- " << crs_per_stu[i][k]
							<< endl;
					return INT_MAX;
				}
				if (distance < 0)
					distance = -distance;
				if (distance == 1)
					c += 16;
				else if (distance == 2)
					c += 8;
				else if (distance == 3)
					c += 4;
				else if (distance == 4)
					c += 2;
				else if (distance == 5)
					c += 1;
//				printf("Student %d courses %d - %d distance =%d \n",
//						i, crs_per_stu[i][j], crs_per_stu[i][k], distance);
			}
		}
//	printf("Student %d penalty %d \n", i, c);
		sum += c;
	}
	return sum;
}

void convertToBoostGraph() {
	typedef boost::property<boost::edge_weight_t, int> EdgeWeightProperty;
	typedef boost::adjacency_list<boost::setS, boost::vecS, boost::undirectedS,
			boost::no_property, EdgeWeightProperty> mygraph;
	typedef boost::graph_traits<mygraph>::vertex_descriptor Vertex;

	mygraph g;
//	std::cout << "Number of edges: " << num_edges(g) << std::endl;
//	std::cout << "Number of vertices: " << num_vertices(g) << std::endl;
	for (int i = 1; i <= crs_no; i++) {
		for (pair<int, int> e : graph[i]) {
//			printf("vertex %d to vertex %d having weight %d\n", i, e.second,
//					e.first);
			boost::add_edge(i, e.second, e.first, g);
			std::cout << "Number of edges: " << num_edges(g) << std::endl;
			std::cout << "Number of vertices: " << num_vertices(g) << std::endl;
		}
	}
	std::cout << "Number of edges: " << num_edges(g) << std::endl;
	std::cout << "Number of vertices: " << num_vertices(g) << std::endl;
	mygraph::vertex_iterator vertexIt, vertexEnd;
	mygraph::adjacency_iterator neighbourIt, neighbourEnd;
	boost::tie(vertexIt, vertexEnd) = vertices(g);
	for (; vertexIt != vertexEnd; ++vertexIt) {
		std::cout << *vertexIt << " is connected with ";
		boost::tie(neighbourIt, neighbourEnd) = adjacent_vertices(*vertexIt, g);
		for (; neighbourIt != neighbourEnd; ++neighbourIt)
			std::cout << *neighbourIt << " ";
		std::cout << "\n";
	}

//	std::vector<Vertex> predecessors(boost::num_vertices(g)); // To store parents
//	std::vector<int> distances(boost::num_vertices(g)); // To store distances
//	int source = 1;
//	boost::dijkstra_shortest_paths(g, source, boost::predecessor_map(&predecessors[0]).distance_map(&distances[0]));
//	for(int i=1;i<=5;i++){
//		 cout << i << " " << distances[i] << endl;
//	}

	int max = 0;
	for (int source = 1; source <= crs_no; source++) {
		std::vector<Vertex> predecessors(boost::num_vertices(g)); // To store parents
		std::vector<int> distances(boost::num_vertices(g)); // To store distances
		boost::dijkstra_shortest_paths(g, source,
				boost::predecessor_map(&predecessors[0]).distance_map(
						&distances[0]));
		for (int i = 1; i <= crs_no; i++) {
			if (distances[i]==INT_MAX)
				continue;
			cout << "Source = " << source << " target = " << i << " shortest path distance =" << distances[i] << endl;
			if (distances[i] > max)
				max = distances[i];
		}
	}
	cout << "The maximum shortest path between two vertices is " << max << endl;
}

void ex03_undirected_graph() {
	// http://www.ibm.com/developerworks/aix/library/au-aix-boost-graph/au-aix-boost-graph-pdf.pdf
	typedef boost::adjacency_list<boost::listS, boost::vecS, boost::undirectedS> mygraph;
	mygraph g;
	boost::add_edge(0, 1, g);
	boost::add_edge(0, 3, g);
	boost::add_edge(1, 2, g);
	boost::add_edge(2, 3, g);
	mygraph::vertex_iterator vertexIt, vertexEnd;
	mygraph::adjacency_iterator neighbourIt, neighbourEnd;
	boost::tie(vertexIt, vertexEnd) = vertices(g);
	for (; vertexIt != vertexEnd; ++vertexIt) {
		std::cout << *vertexIt << " is connected with ";
		boost::tie(neighbourIt, neighbourEnd) = adjacent_vertices(*vertexIt, g);
		for (; neighbourIt != neighbourEnd; ++neighbourIt)
			std::cout << *neighbourIt << " ";
		std::cout << "\n";
	}
}

void load_solution() {
	sol = new int[crs_no + 1] { 0 };
	fstream filestr;
	string buffer;
	filestr.open(sol_fn.c_str());
	if (filestr.is_open()) {
		while (getline(filestr, buffer)) {
			string buffer2;
			stringstream ss;
			ss.str(buffer); // αρχικοποίηση stringstream
			ss >> buffer2;
			int crs_index = atoi(buffer2.c_str());
			ss >> buffer2;
			int period = atoi(buffer2.c_str());
			sol[crs_index] = period;
			printf("Course %d in period %d\n", crs_index, period);
			um_sol[crs_index] = period;
		}
		filestr.close();
		int cost = compute_cost();
		cout << "Το κόστος της λύσης είναι " << cost << " ("
				<< (double) cost / (double) stu_no << ")" << endl;
	} else {
		cerr << "file not found" << endl;
	}
}

int main(int argc, char **argv) {
	int choice;
	string prob;
	do {
		cout << "14/12/2014" << endl;
		cout << "1. Φόρτωση  προβλήματος" << endl;
		cout << "2. Στατιστικά" << endl;
		cout << "3. Γράφος προβλήματος" << endl;
		cout << "4. Φόρτωση λύσης-υπολογισμός κόστους" << endl;
		cout << "5. Έξοδος" << endl;
		cout << "Επιλογή: ";
		cin >> choice;
		switch (choice) {
		case 1:
			cout
					<< "Δώσε όνομα προβλήματος[TINY|CAR91-II|CAR91|KFU93|LSE91|TRE92|UTE92]: ";
			cin >> prob;
			read_problem_data(prob);
			print_conflict_density();
			break;
		case 2:
			student_stats();
			courses_stats();
			break;
		case 3:
			graph_construction();
			cout << "Η διάμετρος είναι: " << graph_diameter() << endl;
			break;
		case 4:
			load_solution();
			break;
		case 5:
			exit(1);
		case 6: {
			// κρυφή επιλογή
			cout << "stress test" << endl;
			list<string> problems;
			problems.push_back("CAR91");
			problems.push_back("KFU93");
			problems.push_back("LSE91");
			problems.push_back("TRE92");
			problems.push_back("UTE92");
			for (string p : problems) {
				read_problem_data(p);
				cout << p << ": ";
				print_conflict_density();
			}
			exit(1);
		}
		case 7: {
			read_problem_data("CAR91-II");
//			read_problem_data("CAR91");
//			read_problem_data("KFU93");
//			read_problem_data("UTE92");
			cout << "enrolments: " << count_enrolments() << endl;
			cout << "enrolments: " << count_enrolments2() << endl;
			graph_construction();
			exit(1);
		}
		case 8: { // test boost
//			ex03_undirected_graph();
			graph_construction();
			convertToBoostGraph();
			exit(1);
		}
		default:
			cout << "Λάθος επιλογή. Προσπαθήστε ξανά!" << endl;
		}
	} while (choice != 5);
}

